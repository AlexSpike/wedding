﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wedding.Model;

namespace Wedding.ViewModel
{
    public class ResumeViewModel
    {
        public long Id { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public DateTime CreateDate { get; set; }

        public long CategoryId { get; set; }

        public string UserId { get; set; }

        public long JobPositionId { get; set; }

        public UserProfile Profile { get; set; }

        public IEnumerable<ResumePhotoViewModel> ResumePhotos { get; set; }
    }
}

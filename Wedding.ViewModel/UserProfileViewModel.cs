﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wedding.ViewModel
{
    public class UserProfileViewModel
    {
        public string FirstName { get; set; }

        public string AvaPath { get; set; }

        public string UserId { get; set; }
    }
}

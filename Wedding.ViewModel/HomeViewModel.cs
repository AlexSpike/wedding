﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wedding.ViewModel
{
    public class HomeViewModel
    {
        public IEnumerable<ResumeViewModel> Resumes { get; set; }
    }
}

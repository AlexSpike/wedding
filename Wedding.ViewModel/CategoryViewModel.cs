﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wedding.Model;

namespace Wedding.ViewModel
{
    public sealed class CategoryViewModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string PhotoUrl { get; set; }

        public string Alias { get; set; }

        public ICollection<ResumeViewModel> Resumes { get; set; }
    }
}
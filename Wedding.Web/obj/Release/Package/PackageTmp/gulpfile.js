﻿var gulp = require('gulp'),
    Q = require('q'),
    sass = require('gulp-sass');
    rimraf = require('rimraf');

gulp.task('copy:lib', ['clean'], function () {
    var libs = [
        "@angular",
        "systemjs",
        "core-js",
        "zone.js",
        "reflect-metadata",
        "rxjs",
        "bootstrap",
        "hammerjs",
        "jquery",
        "owl.carousel",
        "pace-progress"
    ];

    var promises = [];

    libs.forEach(function (lib) {
        var defer = Q.defer();
        var pipeline = gulp
            .src('node_modules/' + lib + '/**/*')
            .pipe(gulp.dest('./js/libs/' + lib));

        pipeline.on('end', function () {
            defer.resolve();
        });
        promises.push(defer.promise);
    });

    return Q.all(promises);
});

gulp.task('sass', function () {
    return gulp.src('./styles/sass/**/*.scss')
      .pipe(sass.sync().on('error', sass.logError))
      .pipe(gulp.dest('./styles'));
});

gulp.task('clean', function (cb) {
    return rimraf('./js/libs/', cb);
});

gulp.task('sass:watch', function () {
    gulp.watch('./styles/sass/**/*.scss', ['sass']);
});
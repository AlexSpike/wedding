﻿(function (global) {
    var map = {
        'app': '/app', // 'dist',
        '@angular': '/js/libs/@angular',
        'rxjs': '/js/libs/rxjs',
        'moment': '/js/libs/moment/min/moment.min.js'
    };
    var packages = {
        'app': { main: 'main.js', defaultExtension: 'js' },
        'rxjs': { defaultExtension: 'js' },
        'moment': { type: 'cjs', defaultExtension: 'js' },
        '@angular/material': {
            format: 'cjs',
            main: 'material.umd.js'
        },
        '.': {
            defaultExtension: 'js'
        }
    };
    var ngPackageNames = [
      'common',
      'compiler',
      'core',
      'forms',
      'http',
      'platform-browser',
      'platform-browser-dynamic',
      'router',
      'router-deprecated',
      'upgrade'
    ];
    function packIndex(pkgName) {
        packages['@angular/' + pkgName] = { main: 'index.js', defaultExtension: 'js' };
    }
    function packUmd(pkgName) {
        packages['@angular/' + pkgName] = { main: '/bundles/' + pkgName + '.umd.js', defaultExtension: 'js' };
    }
    var setPackageConfig = System.packageWithIndex ? packIndex : packUmd;
    ngPackageNames.forEach(setPackageConfig);
    var config = {
        map: map,
        packages: packages,
        paths: {
            'ng2-material/all': 'node_modules/ng2-material/all'
        }
    };
    System.config(config);
})(this);

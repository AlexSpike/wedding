"use strict";
var router_1 = require('@angular/router');
var home_component_1 = require('./home/home.component');
var category_detail_component_1 = require('./category/category-detail/category-detail.component');
var personal_account_routing_1 = require('./personal-account/personal-account.routing');
var routes = [
    {
        path: 'category/:id',
        component: category_detail_component_1.CategoryDetailComponent
    },
    {
        path: '',
        component: home_component_1.HomeComponent
    }
].concat(personal_account_routing_1.PersonalAccountRouting);
// - Updated Export
exports.routing = router_1.RouterModule.forRoot(routes);
//# sourceMappingURL=app.routing.js.map
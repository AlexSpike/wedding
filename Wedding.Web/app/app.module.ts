import { NgModule } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, XHRBackend } from '@angular/http';
import { MaterialModule } from '@angular/material';

import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './account/register/register.component';
import { LoginComponent } from './account/login/login.component';
import { AccountDialog } from './account/account-dialog/account-dialog'
import { CategoryListSidebarComponent } from './category/category-list-sidebar/category-list-sidebar.component';
import { CategoryDetailComponent } from './category/category-detail/category-detail.component';
import { AccountComponent } from './account/account.component';
import { PersonalAccountModule } from './personal-account/personal-account.module';
import { ResumeComponent } from './resume/resume.component';
import { ResumePricePipe } from './category/category-detail/resume-price.pipe';
import {AuthenticationConnectionBackend} from "./shared/authenticated-connection.backend";

@NgModule({
    imports: [
        BrowserModule,
        routing,
        FormsModule,
        HttpModule,
        MaterialModule.forRoot(),
        PersonalAccountModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        AccountDialog,
        CategoryListSidebarComponent,
        CategoryDetailComponent,
        AccountComponent,
        ResumeComponent,
        ResumePricePipe
    ],
    entryComponents: [
        AccountDialog
    ],
    bootstrap: [AppComponent],
    providers: [
        { provide: LocationStrategy, useClass: PathLocationStrategy},
        { provide: XHRBackend, useClass: AuthenticationConnectionBackend}
    ]
})
export class AppModule { }
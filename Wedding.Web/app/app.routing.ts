import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { CategoryDetailComponent } from './category/category-detail/category-detail.component';

import { PersonalAccountRouting } from './personal-account/personal-account.routing';

const routes: Routes = [
    {
        path: 'category/:id',
        component: CategoryDetailComponent
    },
    {
        path: '',
        component: HomeComponent
    },
    ...PersonalAccountRouting
];

// - Updated Export
export const routing = RouterModule.forRoot(routes);
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var resume_service_1 = require("../resume.service");
var resume_model_1 = require("../../../shared/models/resume.model");
var AddResumeComponent = (function () {
    function AddResumeComponent(resumeService, router, formBuilder) {
        this.resumeService = resumeService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.resume = new resume_model_1.Resume();
        this.showSpinner = false;
        this.file_srcs = [];
        this.categoryList();
        this.getJobPosition();
    }
    AddResumeComponent.prototype.ngOnInit = function () {
        this.form = this.formBuilder.group({
            description: ['', forms_1.Validators.required],
            price: ['', forms_1.Validators.required],
            jobPosition: ['', forms_1.Validators.required],
            category: ['', forms_1.Validators.required]
        });
    };
    AddResumeComponent.prototype.categoryList = function () {
        var _this = this;
        this.resumeService.categoryList().subscribe(function (categories) {
            _this.categories = categories;
        }, function (error) { return _this.errorMessage = error; });
    };
    AddResumeComponent.prototype.getJobPosition = function () {
        var _this = this;
        this.resumeService.getJobPosition().subscribe(function (jobPosition) {
            if (jobPosition) {
                _this.jobPositions = jobPosition;
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    AddResumeComponent.prototype.addResume = function (resume) {
        var _this = this;
        if (resume.resumePhotos == null) {
            this.errorMessageForPhoto = "Выберите хотя бы одну фотографию!";
            return;
        }
        this.showSpinner = true;
        this.resumeService.addResume(this.resume).subscribe(function (resume) {
            if (resume) {
                _this.resumeService.makeFileRequest(_this.resume.resumePhotos, resume.id).subscribe(function () {
                    _this.router.navigate(['lk/resume']);
                });
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    AddResumeComponent.prototype.fileChange = function (input) {
        var _this = this;
        this.resume.resumePhotos = [];
        this.errorMessageForPhoto = '';
        this.resume.resumePhotos = input.files;
        this.file_srcs = [];
        for (var i = 0; i < input.files.length; i++) {
            var img = document.createElement("img");
            img.src = window.URL.createObjectURL(input.files[i]);
            var reader = new FileReader();
            reader.addEventListener("load", function (event) {
                img.src = event.target.result;
                _this.file_srcs.push(img.src);
            }, false);
            reader.readAsDataURL(input.files[i]);
        }
    };
    AddResumeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'add-resume',
            templateUrl: 'add-resume.component.html',
            providers: [resume_service_1.ResumeService]
        }), 
        __metadata('design:paramtypes', [resume_service_1.ResumeService, router_1.Router, forms_1.FormBuilder])
    ], AddResumeComponent);
    return AddResumeComponent;
}());
exports.AddResumeComponent = AddResumeComponent;
//# sourceMappingURL=add-resume.component.js.map
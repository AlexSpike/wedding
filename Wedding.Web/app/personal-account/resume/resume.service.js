"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var http_1 = require('@angular/http');
var core_1 = require('@angular/core');
var Rx_1 = require('rxjs/Rx');
var ResumeService = (function () {
    function ResumeService(http) {
        this.http = http;
        this.defaultUrl = 'http://api.wedding-portfolio.of.by/api/personal-account';
        this.getToken();
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        if (this.token) {
            this.headers.append('Authorization', 'Bearer ' + this.token);
        }
        this.options = new http_1.RequestOptions({ headers: this.headers });
    }
    ResumeService.prototype.getToken = function () {
        this.token = localStorage.getItem('token-key');
    };
    ResumeService.prototype.getResume = function () {
        return this.http.get(this.defaultUrl + "/get-resume", this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ResumeService.prototype.getJobPosition = function () {
        return this.http.get(this.defaultUrl + "/get-job-position", this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ResumeService.prototype.addResume = function (resume) {
        return this.http.post(this.defaultUrl + "/resume/add", resume, this.options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ResumeService.prototype.makeFileRequest = function (files, resumeId) {
        var _this = this;
        return Rx_1.Observable.create(function (observer) {
            var formData = new FormData(), xhr = new XMLHttpRequest();
            for (var i = 0; i < files.length; i++) {
                formData.append("uploads[]", files[i]);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        observer.next(xhr.response);
                        observer.complete();
                    }
                    else {
                        observer.error(xhr.response);
                    }
                }
            };
            formData.append("resumeId", resumeId);
            xhr.open('POST', _this.defaultUrl + "/resume/upload", true);
            xhr.setRequestHeader('Authorization', 'Bearer ' + _this.token);
            var serverFileName = xhr.send(formData);
            return serverFileName;
        });
    };
    ResumeService.prototype.categoryList = function () {
        return this.http.get('http://api.wedding-portfolio.of.by/api/category/category-list')
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ResumeService.prototype.deleteResume = function (resumeId) {
        return this.http.delete(this.defaultUrl + '/resume/delete/' + resumeId, this.options);
    };
    ResumeService.prototype.handleError = function (error) {
        var errMsg = error.message || 'Server error';
        console.error(errMsg);
        return Rx_1.Observable.throw(errMsg);
    };
    ResumeService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ResumeService);
    return ResumeService;
}());
exports.ResumeService = ResumeService;
//# sourceMappingURL=resume.service.js.map
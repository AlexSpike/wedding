"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var resume_service_1 = require("./resume.service");
var ResumeComponent = (function () {
    function ResumeComponent(resumeService) {
        this.resumeService = resumeService;
        this.getResume();
    }
    ResumeComponent.prototype.getResume = function () {
        var _this = this;
        this.resumeService.getResume().subscribe(function (resume) {
            if (resume) {
                _this.resume = resume;
                _this.resumePhoto = resume.resumePhotos[0];
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    ResumeComponent.prototype.deleteResume = function (resume) {
        var _this = this;
        this.resumeService.deleteResume(resume.id).subscribe(function (resume) {
            if (resume) {
                _this.resume = null;
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    ResumeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'resume',
            templateUrl: 'resume.component.html',
            providers: [resume_service_1.ResumeService]
        }), 
        __metadata('design:paramtypes', [resume_service_1.ResumeService])
    ], ResumeComponent);
    return ResumeComponent;
}());
exports.ResumeComponent = ResumeComponent;
//# sourceMappingURL=resume.component.js.map
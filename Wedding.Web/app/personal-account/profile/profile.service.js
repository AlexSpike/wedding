"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var http_1 = require('@angular/http');
var core_1 = require('@angular/core');
var Rx_1 = require('rxjs/Rx');
var account_service_1 = require("../../account/account.service");
var ProfileService = (function () {
    function ProfileService(http, accountService) {
        this.http = http;
        this.accountService = accountService;
        this.defaultUrl = 'http://api.wedding-portfolio.of.by/api/personal-account';
        this.getToken();
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        if (this.token) {
            this.headers.append('Authorization', 'Bearer ' + this.token);
        }
        this.options = new http_1.RequestOptions({ headers: this.headers });
    }
    ProfileService.prototype.getToken = function () {
        this.token = localStorage.getItem('token-key');
    };
    ProfileService.prototype.getProfile = function () {
        return this.http.get(this.defaultUrl + "/get-profile", this.options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProfileService.prototype.handleError = function (error) {
        var errMsg = error.message || 'Server error';
        console.error(errMsg);
        return Rx_1.Observable.throw(errMsg);
    };
    ProfileService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, account_service_1.AccountService])
    ], ProfileService);
    return ProfileService;
}());
exports.ProfileService = ProfileService;
//# sourceMappingURL=profile.service.js.map
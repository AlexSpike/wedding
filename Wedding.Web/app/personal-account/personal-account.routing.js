"use strict";
var personal_account_component_1 = require('./personal-account.component');
var profile_component_1 = require('./profile/profile.component');
var resume_component_1 = require('./resume/resume.component');
var add_resume_component_1 = require('./resume/add-resume/add-resume.component');
exports.PersonalAccountRouting = [
    {
        path: 'lk',
        component: personal_account_component_1.PersonalAccountComponent,
        children: [
            { path: 'profile', component: profile_component_1.ProfileComponent },
            { path: 'resume', component: resume_component_1.ResumeComponent },
            { path: 'resume/add', component: add_resume_component_1.AddResumeComponent },
            { path: '', redirectTo: 'profile', pathMatch: 'full' }
        ]
    },
];
//# sourceMappingURL=personal-account.routing.js.map
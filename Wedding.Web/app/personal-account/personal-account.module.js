"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var material_1 = require('@angular/material');
var forms_2 = require('@angular/forms');
var router_1 = require('@angular/router');
var personal_account_component_1 = require('./personal-account.component');
var personal_account_sidebar_component_1 = require('./personal-account-sidebar/personal-account-sidebar.component');
var profile_component_1 = require('./profile/profile.component');
var resume_component_1 = require('./resume/resume.component');
var add_resume_component_1 = require('./resume/add-resume/add-resume.component');
var PersonalAccountModule = (function () {
    function PersonalAccountModule() {
    }
    PersonalAccountModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                router_1.RouterModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                forms_2.ReactiveFormsModule,
                material_1.MaterialModule.forRoot()
            ],
            declarations: [
                personal_account_component_1.PersonalAccountComponent,
                personal_account_sidebar_component_1.PersonalAccountSidebarComponent,
                profile_component_1.ProfileComponent,
                resume_component_1.ResumeComponent,
                add_resume_component_1.AddResumeComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], PersonalAccountModule);
    return PersonalAccountModule;
}());
exports.PersonalAccountModule = PersonalAccountModule;
//# sourceMappingURL=personal-account.module.js.map
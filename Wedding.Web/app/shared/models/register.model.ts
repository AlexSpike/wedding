﻿export class Register {
    constructor(
        public firstName: string,
        public email: string,
        public password: string
    ) { }
}
"use strict";
var Profile = (function () {
    function Profile(id, jobPosition, firstName, avaPath) {
        this.id = id;
        this.jobPosition = jobPosition;
        this.firstName = firstName;
        this.avaPath = avaPath;
    }
    return Profile;
}());
exports.Profile = Profile;
//# sourceMappingURL=profile.model.js.map
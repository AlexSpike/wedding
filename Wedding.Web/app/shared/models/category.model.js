"use strict";
var Category = (function () {
    function Category(id, name, description, photoUrl, alias) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.photoUrl = photoUrl;
        this.alias = alias;
    }
    return Category;
}());
exports.Category = Category;
//# sourceMappingURL=category.model.js.map
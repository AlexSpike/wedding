"use strict";
var Resume = (function () {
    function Resume(id, description, price, createDate, categoryId, userId, jobPositionId, profile, resumePhotos) {
        this.id = id;
        this.description = description;
        this.price = price;
        this.createDate = createDate;
        this.categoryId = categoryId;
        this.userId = userId;
        this.jobPositionId = jobPositionId;
        this.profile = profile;
        this.resumePhotos = resumePhotos;
    }
    return Resume;
}());
exports.Resume = Resume;
//# sourceMappingURL=resume.model.js.map
"use strict";
var JobPosition = (function () {
    function JobPosition(id, name) {
        this.id = id;
        this.name = name;
    }
    return JobPosition;
}());
exports.JobPosition = JobPosition;
//# sourceMappingURL=jobPosition.model.js.map
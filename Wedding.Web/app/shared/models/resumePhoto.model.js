"use strict";
var ResumePhoto = (function () {
    function ResumePhoto(id, photoUrl, resumeId) {
        this.id = id;
        this.photoUrl = photoUrl;
    }
    return ResumePhoto;
}());
exports.ResumePhoto = ResumePhoto;
//# sourceMappingURL=resumePhoto.model.js.map
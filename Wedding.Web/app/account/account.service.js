"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var http_1 = require('@angular/http');
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var user_model_1 = require("../shared/models/user.model");
var AccountService = (function () {
    function AccountService(http, router) {
        this.http = http;
        this.router = router;
        this.defaultUrl = 'http://api.wedding-portfolio.of.by/';
        this.currentUser = new user_model_1.User();
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.options = new http_1.RequestOptions({ headers: this.headers });
        this.headersForLogin = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded;' });
        this.optionsForLogin = new http_1.RequestOptions({ headers: this.headersForLogin });
    }
    AccountService.prototype.register = function (registerUser) {
        return this.http.post(this.defaultUrl + "api/account/register", JSON.stringify(registerUser), this.options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    AccountService.prototype.login = function (loginUser) {
        var body = "grant_type=password&username=" + loginUser.email + "&password=" + loginUser.password;
        return this.http.post(this.defaultUrl + "api/get-token", body, this.optionsForLogin)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    AccountService.prototype.isLogged = function () {
        return !!localStorage.getItem('token-key');
    };
    AccountService.prototype.checkCurrentUser = function () {
        if (!!localStorage.getItem('firstName')) {
            this.currentUser.firstName = localStorage.getItem('firstName');
        }
        if (!!localStorage.getItem('avaUrl')) {
            this.currentUser.avaUrl = localStorage.getItem('avaUrl');
        }
        return this.currentUser;
    };
    AccountService.prototype.setLocalStorage = function (data) {
        localStorage.setItem('token-key', data.access_token);
        localStorage.setItem('email', data.email);
        localStorage.setItem('firstName', data.firstName);
        localStorage.setItem('avaUrl', data.avaUrl);
    };
    AccountService.prototype.logout = function () {
        localStorage.removeItem('token-key');
        localStorage.removeItem('email');
        localStorage.removeItem('firstName');
        localStorage.removeItem('avaUrl');
        this.router.navigate(['']);
    };
    AccountService.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof http_1.Response) {
            var body = error.json();
            if (body.modelState) {
                for (var key in body.modelState) {
                    errMsg = body.modelState[key][0];
                }
            }
            else {
                errMsg = body.error_description;
            }
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Promise.reject(errMsg);
    };
    AccountService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, router_1.Router])
    ], AccountService);
    return AccountService;
}());
exports.AccountService = AccountService;
//# sourceMappingURL=account.service.js.map
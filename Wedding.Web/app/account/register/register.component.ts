﻿import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { AccountService } from "../account.service";
import { Register } from "../../shared/models/register.model";
import { Login } from "../../shared/models/login.model";
import { AccountDialog } from "../account-dialog/account-dialog";

@Component({
    moduleId: module.id,
    selector: 'register',
    templateUrl: 'register.component.html',
    providers: [ AccountService ]
})
export class RegisterComponent {
    constructor(private accountService: AccountService,
                public dialogRef: MdDialogRef<AccountDialog>) {
    }

    errorMessage: string[];
    registerUser = new Register("", "", "");
    loginUser = new Login("", "");

    public register(registerUser: Register) {
        this.accountService.register(registerUser).subscribe(data => {
                this.loginUser.email = data.email;
                this.loginUser.password = data.password;
                this.accountService.login(this.loginUser).subscribe(
                    login => {
                        if (login) {
                            this.accountService.setLocalStorage(login);
                            this.dialogRef.close();
                        }
                    },
                    error => this.errorMessage = <any>error
                );
            },
            error => this.errorMessage = <any>error
        );
    }
}
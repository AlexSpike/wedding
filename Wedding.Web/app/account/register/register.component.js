"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var material_1 = require('@angular/material');
var account_service_1 = require("../account.service");
var register_model_1 = require("../../shared/models/register.model");
var login_model_1 = require("../../shared/models/login.model");
var RegisterComponent = (function () {
    function RegisterComponent(accountService, dialogRef) {
        this.accountService = accountService;
        this.dialogRef = dialogRef;
        this.registerUser = new register_model_1.Register("", "", "");
        this.loginUser = new login_model_1.Login("", "");
    }
    RegisterComponent.prototype.register = function (registerUser) {
        var _this = this;
        this.accountService.register(registerUser).subscribe(function (data) {
            _this.loginUser.email = data.email;
            _this.loginUser.password = data.password;
            _this.accountService.login(_this.loginUser).subscribe(function (login) {
                if (login) {
                    _this.accountService.setLocalStorage(login);
                    _this.dialogRef.close();
                }
            }, function (error) { return _this.errorMessage = error; });
        }, function (error) { return _this.errorMessage = error; });
    };
    RegisterComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'register',
            templateUrl: 'register.component.html',
            providers: [account_service_1.AccountService]
        }), 
        __metadata('design:paramtypes', [account_service_1.AccountService, material_1.MdDialogRef])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map
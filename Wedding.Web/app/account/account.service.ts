import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Register } from "../shared/models/register.model";
import { Login } from "../shared/models/login.model";
import { User } from "../shared/models/user.model";

@Injectable()
export class AccountService {
    private defaultUrl: string = 'http://api.wedding-portfolio.of.by/';

    headers: Headers;
    options: RequestOptions;
    headersForLogin: Headers;
    optionsForLogin: RequestOptions;

    public currentUser: User = new User();

    constructor(private http: Http, private router: Router) {
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        this.options = new RequestOptions({ headers: this.headers });

        this.headersForLogin = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded;' });
        this.optionsForLogin = new RequestOptions({ headers: this.headersForLogin });
    }

    register(registerUser: Register) {
        return this.http.post(this.defaultUrl + "api/account/register", JSON.stringify(registerUser), this.options)
            .map(res => <Register>res.json())
            .catch(this.handleError);
    }

    login(loginUser: Login) {
        let body = "grant_type=password&username=" + loginUser.email + "&password=" + loginUser.password;
        return this.http.post(this.defaultUrl + "api/get-token", body, this.optionsForLogin)
            .map(res => <Login>res.json())
            .catch(this.handleError);
    }

    isLogged(){
        return !!localStorage.getItem('token-key');
    }

    checkCurrentUser() {
        if (!!localStorage.getItem('firstName')){
            this.currentUser.firstName = localStorage.getItem('firstName');
        }
        if (!!localStorage.getItem('avaUrl')){
            this.currentUser.avaUrl = localStorage.getItem('avaUrl');
        }
        return this.currentUser;
    }

    setLocalStorage(data) {
        localStorage.setItem('token-key', data.access_token);
        localStorage.setItem('email', data.email);
        localStorage.setItem('firstName', data.firstName);
        localStorage.setItem('avaUrl', data.avaUrl);
    }

    logout() {
        localStorage.removeItem('token-key');
        localStorage.removeItem('email');
        localStorage.removeItem('firstName');
        localStorage.removeItem('avaUrl');
        this.router.navigate(['']);
    }

    private handleError (error: Response | any) {
        let errMsg: Object;
        if (error instanceof Response) {
            const body = error.json();
            if (body.modelState) {
                for (var key in body.modelState) {
                    errMsg = body.modelState[key][0];
                }
            } else{
                errMsg = body.error_description;
            }
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Promise.reject(errMsg);
    }
}
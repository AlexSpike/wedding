﻿import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { AccountService  } from "../account.service";
import { Login } from "../../shared/models/login.model";
import { AccountDialog } from "../account-dialog/account-dialog"

@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: 'login.component.html',
    providers: [ AccountService ]
})
export class LoginComponent {
    errorMessage: string[];
    loginUser: Login = new Login();

    constructor(private accountService: AccountService,
                public dialogRef: MdDialogRef<AccountDialog>) {
    }

    public login(loginUser: Login) {
        this.accountService.login(this.loginUser).subscribe(
            data => {
                if (data) {
                    this.accountService.setLocalStorage(data);
                    this.dialogRef.close();
                }
            },
            error => this.errorMessage = <any>error
        );
    }
}
import { Component, enableProdMode } from '@angular/core';
import { AccountService } from "./account/account.service";
enableProdMode();
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: 'app.component.html',
    providers: [AccountService ]
})
export class AppComponent {
    constructor(private accountService: AccountService) {
        this.isLogged();
    }

    public isLogged() {
        return this.accountService.isLogged();
    }
}
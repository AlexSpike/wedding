"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var material_1 = require('@angular/material');
var app_routing_1 = require('./app.routing');
var app_component_1 = require('./app.component');
var home_component_1 = require('./home/home.component');
var register_component_1 = require('./account/register/register.component');
var login_component_1 = require('./account/login/login.component');
var account_dialog_1 = require('./account/account-dialog/account-dialog');
var category_list_sidebar_component_1 = require('./category/category-list-sidebar/category-list-sidebar.component');
var category_detail_component_1 = require('./category/category-detail/category-detail.component');
var account_component_1 = require('./account/account.component');
var personal_account_module_1 = require('./personal-account/personal-account.module');
var resume_component_1 = require('./resume/resume.component');
var resume_price_pipe_1 = require('./category/category-detail/resume-price.pipe');
var authenticated_connection_backend_1 = require("./shared/authenticated-connection.backend");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_1.routing,
                forms_1.FormsModule,
                http_1.HttpModule,
                material_1.MaterialModule.forRoot(),
                personal_account_module_1.PersonalAccountModule
            ],
            declarations: [
                app_component_1.AppComponent,
                home_component_1.HomeComponent,
                login_component_1.LoginComponent,
                register_component_1.RegisterComponent,
                account_dialog_1.AccountDialog,
                category_list_sidebar_component_1.CategoryListSidebarComponent,
                category_detail_component_1.CategoryDetailComponent,
                account_component_1.AccountComponent,
                resume_component_1.ResumeComponent,
                resume_price_pipe_1.ResumePricePipe
            ],
            entryComponents: [
                account_dialog_1.AccountDialog
            ],
            bootstrap: [app_component_1.AppComponent],
            providers: [
                { provide: common_1.LocationStrategy, useClass: common_1.PathLocationStrategy },
                { provide: http_1.XHRBackend, useClass: authenticated_connection_backend_1.AuthenticationConnectionBackend }
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map
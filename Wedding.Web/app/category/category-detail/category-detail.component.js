"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var category_service_1 = require("../category.service");
var category_model_1 = require("../../shared/models/category.model");
var CategoryDetailComponent = (function () {
    function CategoryDetailComponent(categoryService, route) {
        this.categoryService = categoryService;
        this.route = route;
        this.category = new category_model_1.Category();
        this.resumes = new Array;
    }
    CategoryDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.currentCategoryAlias = params['id'];
            _this.categoryService.categoryDetail(_this.currentCategoryAlias).subscribe(function (category) {
                _this.category = category;
                _this.resumes = category.resumes;
                _this.findMinAndMaxPrice();
            }, function (error) { return _this.errorMessage = error; });
        });
    };
    CategoryDetailComponent.prototype.findMinAndMaxPrice = function () {
        this.maxPrice = Math.max.apply(Math, this.resumes.map(function (o) { return o.price; }));
        this.minPrice = Math.min.apply(Math, this.resumes.map(function (o) { return o.price; }));
        this.resumePriceFilter = this.maxPrice;
    };
    CategoryDetailComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'category-detail',
            templateUrl: 'category-detail.component.html',
            providers: [category_service_1.CategoryService]
        }), 
        __metadata('design:paramtypes', [category_service_1.CategoryService, router_1.ActivatedRoute])
    ], CategoryDetailComponent);
    return CategoryDetailComponent;
}());
exports.CategoryDetailComponent = CategoryDetailComponent;
//# sourceMappingURL=category-detail.component.js.map
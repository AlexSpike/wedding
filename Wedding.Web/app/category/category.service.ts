import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Category } from "../shared/models/category.model";
import { Observable } from 'rxjs/Rx';

@Injectable()
export class CategoryService {
    private defaultUrl: string = 'http://api.wedding-portfolio.of.by/api/category';
    headers: Headers;
    options: RequestOptions;

    constructor(private http: Http) {
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        this.options = new RequestOptions({ headers: this.headers });
    }

    public categoryList() {
        return this.http.get(this.defaultUrl + '/category-list')
            .map((response: Response) => <Category[]>response.json())
            .catch(this.handleError);
    }

    public categoryDetail(alias:string) {
        return this.http.get(this.defaultUrl + "/" + alias)
            .map((response: Response) => <Category>response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        let errMsg = error.message || 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
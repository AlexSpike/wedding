import { Component } from '@angular/core';
import { CategoryService } from "../category.service";
import { Category } from "../../shared/models/category.model";

@Component({
    moduleId: module.id,
    selector: 'category-list-sidebar',
    templateUrl: 'category-list-sidebar.component.html',
    providers: [ CategoryService ]
})
export class CategoryListSidebarComponent {
    constructor(private categoryService: CategoryService) {
        this.categoryList();
    }

    public categories: Category[];
    errorMessage: string;

    public categoryList() {
        this.categoryService.categoryList().subscribe(
            categories => {
                this.categories = categories;
            },
            error => this.errorMessage = <any>error
        );
    }
}
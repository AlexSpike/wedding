"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var category_service_1 = require("../category.service");
var CategoryListSidebarComponent = (function () {
    function CategoryListSidebarComponent(categoryService) {
        this.categoryService = categoryService;
        this.categoryList();
    }
    CategoryListSidebarComponent.prototype.categoryList = function () {
        var _this = this;
        this.categoryService.categoryList().subscribe(function (categories) {
            _this.categories = categories;
        }, function (error) { return _this.errorMessage = error; });
    };
    CategoryListSidebarComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'category-list-sidebar',
            templateUrl: 'category-list-sidebar.component.html',
            providers: [category_service_1.CategoryService]
        }), 
        __metadata('design:paramtypes', [category_service_1.CategoryService])
    ], CategoryListSidebarComponent);
    return CategoryListSidebarComponent;
}());
exports.CategoryListSidebarComponent = CategoryListSidebarComponent;
//# sourceMappingURL=category-list-sidebar.component.js.map
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wedding.Model;
using Wedding.ViewModel;

namespace Wedding.WebAPI.App_Start
{
    public static class MappingConfig
    {
        public static void RegisterMaps()
        {
            AutoMapper.Mapper.Initialize(config =>
            {
                config.CreateMap<Category, CategoryViewModel>();
                config.CreateMap<UserProfile, UserProfileViewModel>();
                config.CreateMap<Resume, ResumeViewModel>();
                config.CreateMap<ResumePhoto, ResumePhotoViewModel>();
            });
        }
    }
}

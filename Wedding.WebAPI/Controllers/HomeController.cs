﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Wedding.Model;
using Wedding.ViewModel;

namespace Wedding.WebAPI.Controllers
{
    [RoutePrefix("api/home")]
    public class HomeController : ApiController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        [HttpGet]
        [Route("home-resumes")]
        public IHttpActionResult FindRandomResumes()
        {
            var resumes = _db.Resumes.OrderBy(c => Guid.NewGuid()).Take(3).ToList().Select(Mapper.Map<Resume, ResumeViewModel>);

            var resumeViewModels = resumes as IList<ResumeViewModel> ?? resumes.ToList();
            foreach (var resume in resumeViewModels)
            {
                resume.Profile = _db.UserProfiles.FirstOrDefault(r => r.UserId == resume.UserId);
            }

            return Ok(resumeViewModels);
        }
    }
}

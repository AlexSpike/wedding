﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Antlr.Runtime;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Wedding.Model;
using Wedding.ViewModel;
using Wedding.WebAPI.Providers;

namespace Wedding.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/personal-account")]
    public class PersonalAccountController : ApiController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        public ApplicationUserManager UserManager
        {
            get { return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }

        public string UserIdentityId
        {
            get
            {
                var user = UserManager.FindByName(User.Identity.Name);
                return user.Id;
            }
        }

        [HttpGet]
        [Route("get-profile")]
        public IHttpActionResult GetProfile()
        {
            var profile = _db.UserProfiles
                .FirstOrDefault(p => p.UserId == UserIdentityId);
            return Ok(profile);
        }

        [HttpGet]
        [Route("get-resume")]
        public IHttpActionResult GetResume()
        {
            var resume = _db.Resumes.FirstOrDefault(r => r.UserId == UserIdentityId);
            if (resume == null)
            {
                return Ok((Resume) null);
            }
            var model = Mapper.Map<Resume, ResumeViewModel>(resume);
            model.Profile = _db.UserProfiles.FirstOrDefault(p => p.UserId == resume.UserId);
            return Ok(model);
        }

        [Route("get-job-position")]
        [HttpGet]
        public IHttpActionResult GetJobPosition()
        {
            var jobPosition = _db.JobPositions.ToList();
            return Ok(jobPosition);
        }

        [Route("resume/add")]
        [HttpPost]
        public IHttpActionResult AddResume(ResumeViewModel model)
        {
            Resume resume = new Resume
            {
                UserId = UserIdentityId,
                Description = model.Description,
                Price = model.Price,
                CreateDate = DateTime.Now,
                CategoryId = model.CategoryId
            };

            var profile = _db.UserProfiles.FirstOrDefault(p => p.UserId == resume.UserId);
            if (profile != null) profile.JobPositionId = model.JobPositionId;
            _db.Resumes.Add(resume);
            _db.Entry(profile).State = EntityState.Modified;
            _db.SaveChanges();
            return Ok(resume);
        }

        [Route("resume/upload")]
        public async Task<IHttpActionResult> UploadFile()
        {
            var resumePhoto = new ResumePhoto();
            var domain = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority;
            var httpRequest = HttpContext.Current.Request;

            // Read the file and form data.
            MultipartFormDataMemoryStreamProvider provider = new MultipartFormDataMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            string resumeId = provider.FormData["resumeId"];

            if (httpRequest.Files.Count > 0)
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Images/resumes/" + resumeId));
                for (var i = 0; i < httpRequest.Files.Count; i++)
                {
                    var postedFile = HttpContext.Current.Request.Files[i];
                    string directory = "/Images/resumes/" + resumeId + "/" + postedFile.FileName;
                    if (postedFile != null)
                    {
                        var filePath = HttpContext.Current.Server.MapPath(directory);
                        postedFile.SaveAs(filePath);
                        resumePhoto.PhotoUrl = domain + directory;
                        resumePhoto.ResumeId = Int32.Parse(resumeId);
                        _db.ResumePhotos.Add(resumePhoto);
                        _db.SaveChanges();
                    }
                }
            }
            return Ok();
        }

        [Route("resume/delete/{id}")]
        [HttpDelete]
        public IHttpActionResult AddResume(long id)
        {
            var resume = _db.Resumes.Find(id);
            if (resume != null)
            {
                _db.Resumes.Remove(resume);
                _db.SaveChanges();
            }
            return Ok();
        }
    }
}
﻿using System.Linq;
using System.Web.Http;
using Wedding.Model;

namespace Wedding.WebAPI.Controllers
{
    [RoutePrefix("api/resume")]
    public class ResumeController : ApiController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        [HttpGet]
        public IHttpActionResult ResumeList()
        {
            var resumes = _db.Resumes.ToList();
            return Ok(resumes);
        }
    }
}
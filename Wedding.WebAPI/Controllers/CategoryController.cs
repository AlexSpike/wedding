﻿using System.Data.Entity;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using Wedding.Model;
using Wedding.ViewModel;

namespace Wedding.WebAPI.Controllers
{
    [RoutePrefix("api/category")]
    public class CategoryController : ApiController
    {

        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        [HttpGet]
        [Route("category-list")]
        public IHttpActionResult Category()
        {
            var categories = _db.Categories.ToList();
            return Ok(categories);
        }

        [HttpGet]
        [Route("{alias}")]
        public IHttpActionResult CategoryDetail(string alias)
        {
            if (alias == null) return BadRequest();
            
            var category = _db.Categories.Include(c => c.Resumes.Select(r => r.ResumePhotos)).FirstOrDefault(c => c.Alias == alias);
            CategoryViewModel model = Mapper.Map<Category, CategoryViewModel>(category);
            foreach (var resume in model.Resumes)
            {
                resume.Profile = _db.UserProfiles.FirstOrDefault(p => p.UserId == resume.UserId);
            }
            return Ok(model);
        }
    }
}

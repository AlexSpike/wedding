﻿using System;
using System.Collections.Generic;

namespace Wedding.Model
{
    public class Resume : BaseEntity
    {
        public string Description { get; set; }

        public double Price { get; set; }

        public DateTime CreateDate { get; set; }

        public long CategoryId { get; set; }

        public string UserId { get; set; }

        public Category Category { get; set; }

        public ApplicationUser User { get; set; }

        public virtual ICollection<ResumePhoto> ResumePhotos { get; set; }
    }
}

﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Wedding.Model
{

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("WeddingConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Resume> Resumes { get; set; }

        public DbSet<ResumePhoto> ResumePhotos { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<JobPosition> JobPositions { get; set; }

        public DbSet<UserProfile> UserProfiles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Category>()
                .HasMany<Resume>(s => s.Resumes)
                .WithRequired(s => s.Category);

            modelBuilder.Entity<Resume>()
                .HasRequired<Category>(s => s.Category)
                .WithMany(s => s.Resumes)
                .HasForeignKey(s => s.CategoryId);

            modelBuilder.Entity<ApplicationUser>()
                .HasOptional(u => u.Profile)
                .WithOptionalDependent(p => p.User);

            modelBuilder.Entity<UserProfile>()
                .HasRequired(p => p.User)
                .WithMany()
                .HasForeignKey(u => u.UserId);

            modelBuilder.Entity<UserProfile>()
                .HasOptional(s => s.JobPosition)
                .WithMany(s => s.UserProfiles)
                .HasForeignKey(s => s.JobPositionId);

        }
    }
}
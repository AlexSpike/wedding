﻿namespace Wedding.Model
{
    public class UserProfile : BaseEntity
    {
        public string FirstName { get; set; }

        public string AvaPath { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        public long? JobPositionId { get; set; }

        public virtual JobPosition JobPosition { get; set; }
    }
}
﻿namespace Wedding.Model
{
    public class ResumePhoto : BaseEntity
    {
        public string PhotoUrl { get; set; }

        public long ResumeId { get; set; }

        public Resume Resume { get; set; }
    }
}

﻿using System;

namespace Wedding.Model
{
    public class Message : BaseEntity
    {
        public DateTime Data { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string Title { get; set; }
        
        public string Content { get; set; }

        public bool Flag { get; set; }

        public ApplicationUser User { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Wedding.Model
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string PhotoUrl { get; set; }

        public string Alias { get; set; }
        
        public virtual ICollection<Resume> Resumes { get; set; }
    }
}
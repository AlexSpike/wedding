﻿using System.Collections.Generic;

namespace Wedding.Model
{
    public class JobPosition : BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}
